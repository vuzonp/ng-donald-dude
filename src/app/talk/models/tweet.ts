export class Tweet {
  id: string;
  text: string;
}

export class ReadRecentTweetsDto {
  data: Tweet[];
  meta: {
    newest_id: string;
    next_token: string;
    oldest_id: string;
    result_count: number;
  };
}
