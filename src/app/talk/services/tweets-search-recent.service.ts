import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, switchMapTo, tap, timeout } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { ReadRecentTweetsDto, Tweet } from '../models/tweet';

@Injectable()
export class TweetsSearchRecentService {

  private readonly tweets$ = new BehaviorSubject<Tweet[]>([]);

  private readonly endpoint = environment.apiEndpoint;
  private nextToken: string;
  private oldestId: string;

  constructor(
    private readonly httpClient: HttpClient
  ) { }

  get uri(): string {
    const query = new URLSearchParams();
    if (this.nextToken) {
      query.append('next', this.nextToken);
    }
    if (this.oldestId) {
      query.append('oldest_id', this.oldestId);
    }
    return `${this.endpoint}?${query.toString()}`;
  }

  findAny(): Observable<Tweet[]> {
    return this.httpClient.get(this.uri).pipe(
      timeout(5000),
      tap((rawData: ReadRecentTweetsDto) => {
        // Keeps meta data for next request.
        this.oldestId = rawData.meta.oldest_id;
        this.nextToken = rawData.meta.next_token;
        // Merge new tweets in the pre-existing collection.
        this.tweets$.next([
          ...this.tweets$.value,
          ...rawData.data]
        );
      }),
      switchMapTo(this.tweets$.asObservable()),
      catchError((err) => {
        console.error(err);
        return of<Tweet[]>([{
          id: 'err',
          text: 'Oups, les démocrates censurent Donald, le serveur Twitter ne répond pas.'
        }]);
      }),
    );
  }

}
