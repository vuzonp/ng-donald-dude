import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';

import { of, Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';

import { IntersectionService } from 'src/app/shared/services/intersection.service';
import { Tweet } from './models/tweet';
import { TweetsSearchRecentService } from './services/tweets-search-recent.service';

@Component({
  selector: 'app-talk',
  templateUrl: './talk.component.html',
  styleUrls: ['./talk.component.scss']
})
export class TalkComponent implements OnInit, OnDestroy {

  public tweets: Tweet[] = [];
  private sub$: Subscription;

  constructor(
    private readonly hostElement: ElementRef,
    private readonly intersectionService: IntersectionService,
    private readonly tweetsService: TweetsSearchRecentService
  ) { }

  ngOnInit(): void {
    this.updateTweets();
    this.intersectionService.on(() => {
      this.updateTweets();
    });
  }

  public toggleTrollFace(evt: any): void {
    evt.currentTarget.classList.toggle('troll-face');
  }

  /**
   * Refreshes tweets with new call to API.
   */
  private updateTweets(): void {
    this.sub$ = this.tweetsService.findAny().subscribe((tweets) => {
      tweets.forEach((tweet) => {
        this.tweets.push(tweet);
      });
      this.sub$.unsubscribe();
      this.listenLastTweet();
    });
  }

  /**
   * Attaches a trigger to API to the last loaded tweet
   * @todo Use childView
   */
  private listenLastTweet(): void {
    of(true).pipe(
      delay(600),
    ).subscribe(() => {
      const tweets = this.hostElement.nativeElement.querySelectorAll('app-message');
      if (tweets) {
        const children = Array.from(tweets);
        const lastElem = children.pop();
        this.intersectionService.revokeAll();
        this.intersectionService.attachTrigger(lastElem);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.sub$) {
      this.sub$.unsubscribe();
    }
  }

}
