import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { TweetsSearchRecentService } from './services/tweets-search-recent.service';

import { ComicBubbleComponent } from './comic-bubble/comic-bubble.component';
import { TalkComponent } from './talk.component';
import { MessageComponent } from './message/message.component';
import { LoaderComponent } from './loader/loader.component';
import { StarComponent } from './loader/star/star.component';

@NgModule({
  declarations: [
    ComicBubbleComponent,
    TalkComponent,
    MessageComponent,
    LoaderComponent,
    StarComponent
  ],
  imports: [
    SharedModule
  ],
  providers: [
    TweetsSearchRecentService
  ],
  exports: [
    TalkComponent
  ]
})
export class TalkModule { }
