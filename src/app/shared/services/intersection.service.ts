import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IntersectionService {

  private isBrowser = false;
  private readonly intersectionObserver = null;
  private readonly elements = [];
  private readonly callbacks = [];

  constructor(
    @Inject(PLATFORM_ID) plateformId: Object
  ) {
    this.isBrowser = isPlatformBrowser(plateformId);
    if (this.isBrowser) {
      this.intersectionObserver = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
          if (entry.intersectionRatio <= 0) {
            return true;
          }
          // When event is triggered, runs the callback functions.
          this.callbacks.forEach((cb) => cb(entry, entries));
        });
      });
    }
  }

  /** On interaction event, runs callback */
  on(callback): void {
    this.callbacks.push(callback);
  }

  /** Listens interaction from an element.  */
  attachTrigger(elem): void {
    this.elements.push(elem);
    if (this.isBrowser) {
      this.intersectionObserver.observe(elem);
    }
  }

  /** Stop to listen interaction from an element. */
  detachTrigger(elem): void {
    const index = this.elements.findIndex(el => elem.isSameNode(el));
    if (index > -1) {
      if (this.isBrowser) {
        this.intersectionObserver.unobserve(elem);
      }
      this.elements.splice(index, 1);
    }
  }

  /** Stop to listen all interactions from elements. */
  revokeAll(): void {
    Array.from(this.elements).forEach((elem) => {
      this.detachTrigger(elem);
    });
  }

}
