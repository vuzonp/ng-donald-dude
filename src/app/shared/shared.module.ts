import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutoLinkPipe } from './pipes/auto-link.pipe';

@NgModule({
  declarations: [
    AutoLinkPipe,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    CommonModule,
    AutoLinkPipe,
  ]
})
export class SharedModule { }
