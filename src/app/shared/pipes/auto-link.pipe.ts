import { Pipe, PipeTransform } from '@angular/core';
import Autolinker from 'autolinker';

@Pipe({
  name: 'autoLink'
})
export class AutoLinkPipe implements PipeTransform {

  transform(value: string, options: any = {}): string {
    const tweetWithMentions =  value.replace(/(@[a-z])+/, 'twitter.com/$1');
    return Autolinker.link( tweetWithMentions, options);
  }

}
